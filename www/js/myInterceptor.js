﻿(function (app) {
    app.factory('httpRequestInterceptor', ['$q', '$rootScope', '$timeout',
    function ($q, $rootScope, $timeout) {
        return {
            response: function (response) {
                return response || $q.when(response);
            },
            responseError: function (rejection) {
                return rejection || $q.reject(rejection);
            },
            request: function (request) {
                if (request.url != null && request.url.indexOf("/AuthorizationToken") > -1) {
                    request.headers['Login-Token'] = "1-Login";
                }else{
                    request.headers['Authorization-Token'] = window.localStorage.getItem('Token');
                }
                return request || $q.when(request);
            },
            requestError: function (rejection) {
                $q.reject(rejection);
             }
        };
    }]);
})(appMain);