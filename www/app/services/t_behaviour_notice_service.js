(function (app) {

    function Service($http, $q,APIUrl) {
        this._url=APIUrl;
        this._$http = $http;
        this._$q = $q;
    }

    Service.prototype.postHomework = function (homework) {
        var deferred = this._$q.defer();
        var url = this._url+"HomeWork";
           this._$http({
                 method: 'POST',
                 url: url,
                 data: homework,
           }).success(function (data, status, headers, cfg) {
                deferred.resolve(data);
            }).error(function (err, status) {
                alert(err)
                deferred.reject(status);
            });
            return deferred.promise;
    };
    Service.$inject = ['$http', '$q','APIUrl'];
    app.service("PostBehaviourNoticeService", Service);
})(appMain)