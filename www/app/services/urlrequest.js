(function (app) {

    function Service($http, $q, APIUrl) {
        this._url = APIUrl;
        this._$http = $http;
        this._$q = $q;
    }
    
    // post call on given url
    Service.prototype.post = function (postUrl, content) {
        var deferred = this._$q.defer();
        var url=this._url+postUrl;
        this._$http({
            method: 'POST',
            url: url,
            data: content,
        }).success(function (data) {
            deferred.resolve(data);
        }).error(function (err, status) {
            console.log('ERROR');
            console.log(err);
            deferred.reject(status);
        });
        return deferred.promise;
    };

    // Get request on given url
    Service.prototype.get = function (getUrl) {
        var deferred = this._$q.defer();
        var url = this._url+getUrl;  
        this._$http({
            method: 'GET',
            url: url,
        }).success(function (data, status, headers, cfg) {
            deferred.resolve(data);
        }).error(function (err, status) {
            console.log(err)
            deferred.reject(status);
        });
        return deferred.promise;
    };

    Service.$inject = ['$http', '$q','APIUrl'];
    app.service("PostCallService", Service);
})(appMain)
