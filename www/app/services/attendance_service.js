(function (app) {

    function Service($http, $q) {
        this._$http = $http;
        this._$q = $q;
    }
    // method to get menu_list to display on dashboard based on given data.
    Service.prototype.get_monthly_attendance = function (months) {
        var deferred = this._$q.defer();
        var data = window.localStorage.getItem('Token')
        var url = "http://school.jmsofttech.com/api/AttendanceCalendarView/"+months;
           this._$http({
                method: 'GET',
                url: url,
            }).success(function (data, status, headers, cfg) {
                deferred.resolve(data);
            }).error(function (err, status) {
                alert(err)
                deferred.reject(status);
            });
            return deferred.promise;
    };

    Service.$inject = ['$http', '$q'];

    app.service("AttendanceService", Service);
})(appMain) 