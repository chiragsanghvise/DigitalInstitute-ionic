(function (app) {

    function Service($http, $q,APIUrl) {
        this._url=APIUrl;
        this._$http = $http;
        this._$q = $q;
    }

    Service.prototype.sendComplainMessage = function (complain) {
        var deferred = this._$q.defer();
        var url = this._url+"ParentComplain";
           this._$http({
                 method: 'POST',
                 url: url,
                 data: complain,
           }).success(function (data, status, headers, cfg) {
                deferred.resolve(data);
            }).error(function (err, status) {
                alert(err)
                deferred.reject(status);
            });
            return deferred.promise;
    };
    Service.$inject = ['$http', '$q','APIUrl'];
    app.service("ComplainBoxService", Service);
})(appMain)