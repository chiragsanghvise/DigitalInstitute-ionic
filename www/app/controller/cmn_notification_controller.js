angular.module('starter')

.controller('CmnNotification', function($scope, $state,$http ,$ionicPopup,$ionicLoading,PostCallService,APIUrl) {
	
	$ionicLoading.show();


	PostCallService.get('ViewNotification').then(function(notify){
		$ionicLoading.hide();
			if(notify!="[]"){
				$scope.hasNotification=true;
				$scope.notification_list=notify;
			}else{
				$scope.hasNotification=false;
				$ionicPopup.alert({
					template:"Notification is not available",					
				});
			}
		},function(error){
			$ionicLoading.hide();
			alert("error");
		
	});
})