angular.module('starter')
.controller('TTodaySchedule', function($scope, $ionicPopup, $http, $ionicLoading, DateTimeService, ionicDatePicker, APIUrl, PostCallService) {
	/*var noOfDays=9668;*/
	var days=-1;
	$scope.currentDate = 'Select Date';
	$scope.hasSchedule = false;

	//global variables
	var selectedDate = null;
	var selectedBatch = null;

	// call service method to get menu_list and pass it to html page
    PostCallService.get('batch').then(function(batches){
        $scope.batchList = batches;
    },function(msg){
        console.log("ctrl","msg");
        alert(msg);
    });

    $scope.saveBatch = function(batch){
    	selectedBatch = batch;
    	$scope.isBatchSelected = true;
    }

	function getCurrentDate(date){
		var year = date.getFullYear();
        var month = date.getMonth();
        var day = date.getDate();
        return year + "-" + month + "-" + day;
	}
	
	function getNoOfDays(date){
		date = new Date(date.getFullYear(),date.getMonth(),date.getDate());
		// Assuming date is a string of parsable format: ie. "2014-01-27T01:00:00+00:00"
		var diff = date - new Date('1990-01-01 00:00:00');
		// Calculate from milliseconds
		return ((((diff / 1000) / 60) / 60) / 24);
	}

	$scope.getScheduleData = function(noOfDays){
		if(!noOfDays){
			noOfDays = getNoOfDays(new Date());
		}
		days=noOfDays;
		$ionicLoading.show();
		var url = "ViewMyLectures/" + selectedBatch.Id+"/" + days;
		PostCallService.get(url).then(function(data){
			$ionicLoading.hide();
			if(JSON.stringify(data)!="[]"){
				$scope.hasSchedule=true;
				$scope.schedule_list=data;
			}else{
				$ionicPopup.alert({
					template:"No schedule for this date"
				});	
				$scope.hasSchedule=false;
			}
		},function(msg){
			$ionicLoading.hide();
			console.log("error");
		});
	}

	$scope.preDaySchedule= function(){
		days=days-1;
		$scope.getScheduleData(days);
		var date = new Date(1990,00,01);
		date.setDate(date.getDate() + days); 
        $scope.currentDate = getCurrentDate(date);
	};

	$scope.nextDaySchedule= function(){
		days=days + 1;
		$scope.getScheduleData(days);
		var date = new Date(1990,00,01);
		date.setDate(date.getDate() + days); 
        $scope.currentDate = getCurrentDate(date);
	};

	var ipObj1 = {
      	callback: function (val) {  //Mandatory
        	selectedDate = new Date(val);
        	$scope.getScheduleData(getNoOfDays(selectedDate));
        	$scope.currentDate = DateTimeService.getDateString(selectedDate);
      	},
		inputDate: new Date(),      //Optional
		mondayFirst: true,          //Optional
		closeOnSelect: false,       //Optional
		templateType: 'popup'       //Optional
    };

    $scope.openDatePicker = function(){
      	ionicDatePicker.openDatePicker(ipObj1);
    };
})