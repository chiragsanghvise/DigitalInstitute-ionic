(function(){
    angular.module('starter')
    .controller('AttendanceCtrl', function($scope,PostCallService,$ionicLoading) {
        // Variables
        $scope.currentMonth = new Date().getMonth();
        $scope.currentYear = new Date().getFullYear();
        $scope.firstDayOfMonth = new Date($scope.currentYear,$scope.currentMonth,1).getDay();
        $scope.weeks = ['Sun','Mon','Tues','Wed','Thurs','Fri','Sat'];
        $scope.monthNames = ["Jan", "Feb", "March", "April", "May", "June",
                            "July", "Aug", "Sept", "Oct", "Nov", "Dec"]; 
        $scope.NoOfmonths = monthDiff(new Date(1990, 0, 1),new Date($scope.currentYear,$scope.currentMonth,1));
        $scope.attendanceDict = {};
        $scope.lastDayUsed = 1;
        $scope.columnColors = ['grey-font','green-bg','red-bg','orange-bg','blue-bg'];
        
        // Get difference between two months
        function monthDiff(fromDate, toDate) {
            var months;
            months = (toDate.getFullYear() - fromDate.getFullYear()) * 12;
            months -= (fromDate.getMonth() + 1);
            months += toDate.getMonth() + 1;
            return months <= 0 ? 0 : months;
        }
        

        function updateCalender(){
            $scope.NoOfmonths = monthDiff(new Date(1990, 0, 1),new Date($scope.currentYear,$scope.currentMonth,1));
            $scope.firstDayOfMonth = new Date($scope.currentYear,$scope.currentMonth,1).getDay();
            $scope.getMonthlyAttendance();
        }

        // Get number of days in given month
        function daysInMonth(month,year) {
            return new Date(year, month, 0).getDate();
        }

        // Get monthly attendance details and set required fields
        
        $scope.getMonthlyAttendance = function(){
            $ionicLoading.show();
            var att_url='AttendanceCalendarView/'+ $scope.NoOfmonths;
            PostCallService.get(att_url).then(function(monthlyAttendance){
                $ionicLoading.hide();
                $scope.monthlyAttendance = monthlyAttendance;
                $scope.totalNumOfDays = monthlyAttendance.length;
                $scope.get_attendance_list();  
                $scope.attendanceRange = $scope.range();
            },function(msg){
                console.log("ctrl","msg");
                alert(msg);
            });
        }

        // Dictionary that stores date with its attendance status
        $scope.get_attendance_list = function(){
            var weekName = $scope.weeks[$scope.firstDayOfMonth];
            for(var i=0;i<$scope.totalNumOfDays;i++){
                $scope.attendanceDict[i+1] = $scope.monthlyAttendance[i]["Status"];
            }
        }
      
        // Returns calender date
        $scope.range = function(){
            var rows = {};
            var days = 1;
            var nextMonth=1;
            var prevMonthDays = daysInMonth($scope.currentMonth,$scope.currentYear);
            var count = prevMonthDays - $scope.firstDayOfMonth + 1;
            for (var rowNo=0;rowNo<6;rowNo++){
                var ratings = []; 
                var counter = 0;
                // Add space on back to make it disable
                for (var prev_days = 0;rowNo == 0 & prev_days < $scope.firstDayOfMonth; prev_days++,count++) { 
                    ratings.push(count+" ")
                    counter++; 
                }
              
                for(var currDays=days;counter<7 && currDays<=$scope.totalNumOfDays;currDays++,counter++){
                    ratings.push(currDays);
                    days++;
                }

                // Add space on back to make it disable
                for(var nxtDays=nextMonth;counter<7;counter++,nxtDays++,nextMonth++){
                    ratings.push(nxtDays+" ");
                }
                rows[rowNo]=ratings;
            }
            return rows;
        }


        $scope.dateFormatter = function(date){
            return date;
        }

        $scope.go_backward = function(){
            if($scope.currentMonth == 0){
                $scope.currentMonth = 11;
                $scope.currentYear--;
            }else{
                $scope.currentMonth--;
            }
            updateCalender();
        }

        $scope.go_forward = function(){
            if($scope.currentMonth == 11){
                $scope.currentMonth = 0;
                $scope.currentYear++;
            }else{
                $scope.currentMonth++;
            }
            updateCalender();
        }
    })
})();
 
