angular.module('starter')

.controller('TExammarkCtrl', function($scope, $state, $ionicPopup, $location, $ionicLoading, TExammarkService,APIUrl, PostCallService) {

	$scope.standard = "";
	$scope.chapters = [];
	$scope.std_div = "";
	$scope.hasBatches = false;
	$scope.hasExamschedule = false;
	$scope.hasStudentList = false;
	$scope.students = {};

	// Global variables
	var selectedBatch = null;
	var selectedCourse = null;
	var selectedSchedule = null;
	// call service method to get menu_list and pass it to html page
	
	PostCallService.get("course").then(function(courses){
		if (courses.length) {
			$scope.courseList = courses;
			$scope.hasCourses=true;	
		}else{
			$scope.hasCourses=false;
			$scope.hasBatches=false;
			$scope.hasExamschedule = false;
			$scope.hasStudentList = false;
			$ionicPopup.alert({
				template:"Standard Data is not available",					
			});
		};
		
	},function(msg){
		console.log("ctrl","msg");
		alert(msg);
	});

	$scope.getBatchDetails = function(course){
		selectedCourse = course;
		var url = "Batch/"+course.Id;
		$ionicLoading.show(); 
		PostCallService.get(url).then(function(batches){
			$ionicLoading.hide(); 
			if (batches.length) {
				$scope.hasBatches=true;
				$scope.batchList = batches;
			}else{
				$scope.hasBatches=false;
				$scope.hasExamschedule = false;
				$scope.hasStudentList = false;
				$ionicPopup.alert({
					template:"Batch Details is not available",					
				});
			};
			
		},function(msg){
			console.log("ctrl","msg");
			alert(msg);
		});
	}

	$scope.getExamschedule = function(batch){
		selectedBatch = batch;
		$ionicLoading.show();
		var url = "ExamSchedule/"+ batch.Id;
		PostCallService.get(url).then(function(examSchedule){
			$ionicLoading.hide();
			if (batches.length) {
				$scope.hasExamschedule=true;
				$scope.examScheduleList = examSchedule;
			}else{
				$scope.hasExamschedule = false;
				$scope.hasStudentList = false;
				$ionicPopup.alert({
					template:"Exam Data is not available",					
				});
			};
		},function(msg){
			console.log("ctrl","msg");
			alert(msg);
		});
	}
	
	$scope.getStudentList = function(scheduleDetails){
		selectedSchedule = scheduleDetails;
		$ionicLoading.show();
		var url = "Student/" + selectedSchedule.Id + "/" + selectedBatch.Id;		
		PostCallService.get(url).then(function(marksDetail){
			$ionicLoading.hide();
			if(marksDetail.length){
				$scope.hasStudentList = true;
				$scope.marksDetail = marksDetail;
				$scope.studentList = marksDetail['Students'];		
			}else{
				$scope.hasStudentList = false;
				$ionicPopup.alert({
					template:"No stduent registered for this exam",					
				});			
			}
			

		},function(msg){
			console.log("ctrl","msg");
			alert(msg);
		});

	}

	$scope.updateMarks = function(studentId){
		for (var i = 0; i < $scope.studentList.length; i++) {
		//	console.log($scope.marks_detail['Students'][i]); 
			if($scope.studentList[i]['StudentId'] == studentId){
				$scope.marksDetail['Students'][i]['TheoryMarks'] = $scope.students[studentId];
			}
		};
	}


	$scope.sendData = function(){
		$ionicLoading.show();
		PostCallService.post("StudentMarks",$scope.marksDetail).then(function(response){
			$ionicLoading.hide();
		},function(msg){
			console.log("ctrl","msg");
			alert(msg);
		});	
	}

	
	$scope.get_exam_schedule = function(data){
		
		$scope.exam_id = data['Id'];
		var info = {
			'std':$scope.standard,
			'exam_id':data['Id']
		};

		PostCallService.get("ExamSchedule/" + info['std'] + "/"+info['exam_id']).then(function(schedule_list){
			
			alert(JSON.stringify(schedule_list));	
			if (JSON.stringify(schedule_list)!="[]") {
				$scope.is_exam_selected = true;
				$scope.schedule_list = schedule_list;	
			}else{
				$scope.is_exam_selected = false;
				$scope.is_schedule_selected = false;	
				$ionicPopup.alert({
					template:"Exam Schedule is not Declared",					
				});
			};
			
		},function(msg){
			console.log("ctrl","msg");
			alert(msg);
		});		
	}


})
 
