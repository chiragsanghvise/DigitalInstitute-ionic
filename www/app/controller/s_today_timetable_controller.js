angular.module('starter')

.controller('STodayTimetable', function($scope, $state,$http ,$ionicPopup,$ionicLoading,PostCallService,APIUrl) {
	$scope.day_list=['Sun','Mon','Tue','Wed','Thu','Fri','Sat'];
	
	$scope.weekdays=$scope.day_list[ new Date().getDay()];

	var getId= function(weekdays){
		switch(weekdays){
			case 'Sun':
				return 0;
			case 'Mon':
				return 1;
			case 'Tue':
				return 2;
			case 'Wed':
				return 3;
			case 'Thu':
				return 4;
			case 'Fri':
				return 5;
			case 'Sat':
				return 6;
			default:
				return -1;							
		}
	}

	var fetchSchedule=function(url){
		$ionicLoading.show();

		PostCallService.get(url).then(function(timetableData){
			$ionicLoading.hide();
				if(JSON.stringify(timetableData)=="[]"){
					$ionicPopup.alert({
						template:"No schedule for this day",					
					});
					$scope.isLectureAvailable=false;
				}else{
					$scope.lecture_schedule=timetableData;
					$scope.isLectureAvailable=true;
				}
			},function(error){
				$ionicLoading.hide();
				alert(err);	
		});
	}

	var cur_dataUrl="StudentTimeTable/"+getId($scope.weekdays);
	fetchSchedule(cur_dataUrl);
	$scope.getDaySchedule=function(weekdays){
		var schedule_url='StudentTimeTable/'+getId(weekdays);
		fetchSchedule(schedule_url);
	}
})