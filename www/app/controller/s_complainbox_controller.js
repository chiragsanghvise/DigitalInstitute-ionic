angular.module('starter')

.controller('SComplainBox', function($scope, $state,$http ,$ionicPopup,$ionicLoading,PostCallService,ComplainBoxService) {
	$scope.sendComplain=function(complain){
		//$ionicLoading.show();
		if (complain.message=="") {
			$ionicPopup.alert({
				template:"Please enter complain",					
			});
		}else{
			var complainUrl="ParentComplain";
			PostCallService.post(complainUrl,complain).then(function(status){
				$ionicLoading.hide();
				$ionicPopup.alert({
					title:'Success',
					template:status,					
				});
			});
			complain.message="";
		}	
	}
})