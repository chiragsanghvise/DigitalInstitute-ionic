angular.module('starter')

.controller('DashCtrl', function($scope, $state, $ionicPopup,$ionicLoading, $window, $location, $rootScope, dashService) {
	// call service method to get menu_list and pass it to html page
	var usertype=window.localStorage.getItem('UserType');
	$ionicLoading.show();
	dashService.get_names(usertype).then(function(dashboard_data){					
		$ionicLoading.hide();
		$rootScope.menu_list = dashboard_data; 	
		console.log(JSON.stringify(dashboard_data));
		$scope.widthinPX = Math.floor($window.innerWidth / 4);
	},function(msg){
		console.log("ctrl","msg");
		alert(msg);
	});

	// based on selected menu, redirect to module	
	$scope.get_menu_list_item = function(data) {
		menu_type = dashService.get_selected_menu_id(data)		
		console.log(menu_type);
		switch(menu_type){
			// if user selects attendance, redirect to attendance module
			case 1:
				$location.path('/t_attendance');
				break;
			case 2:
				$location.path('/teacherhomework');
				break;	
			case 3:
				$location.path('/stduentbehaviournotice');
				break;
			case 4:
				$location.path('/teacherlogbook');
				break;
			case 5:
				$location.path('/teacherschedule');
				break;
			case 6:
				$location.path('/t_exammark');
				break;

			case 8:
				$location.path('/attendance');
				break;
			case 9:
				$location.path('/examtimetable');
				break;
			case 10:
				$location.path('/stud_result');
				break;
			case 11:
				$location.path('/stud_view_complain');
				break;
			case 12:
				$location.path('/homework');
				break;
			case 13:
				$location.path('/holidaylist');
				break;
			case 14:
				$location.path('/inquiry');
				window.localStorage.setItem('Type', 1);
				break;	
			case 15:
				$location.path('/admission');
				window.localStorage.setItem('Type', 2);
				break;
			case 16:
				$location.path('/income');
				window.localStorage.setItem('Type', 3);
				break;	
			case 17:
				$location.path('/profitloss');
				window.localStorage.setItem('Type', 4);
				break;		
			case 18:
				$location.path('/timetable');
				break;
			case 19:
				$location.path('/logout');	
				break;		
			default:
				$location.path('/dashboard');
		}	
	}	 
})
 
