angular.module('starter')

.controller('SHomework', function($scope, $state,$http,$ionicPopup,$ionicLoading, ionicDatePicker,PostCallService,APIUrl) {
	$ionicLoading.show();
	var curMonth = new Date().getMonth()+1;
	var curYear = new Date().getFullYear();
	var curDay = new Date().getDate();
	$scope.currentDate = curDay + " - " + curMonth + " - " +  curYear;
	var diff = new Date(curYear,curMonth,curDay) - new Date('1990-01-01 00:00:00');
	var numOfdays = ((((diff / 1000) / 60) / 60) / 24);;

	$scope.getStudentHomework = function(){
		var homeworkUrl='ViewStudentHomeWork/'+numOfdays;
		PostCallService.get(homeworkUrl).then(function(homeWork){
			$ionicLoading.hide();
			if(JSON.stringify(homeWork)=="[]"){
				homework_available=true;				
				$ionicPopup.alert({
					template:"No homeWork for this day",					
				});		
			}else{
				$scope.homework_data=homeWork;
				homework_available=false;
			}
			},function(error){
				$ionicLoading.hide();
				console.log("error");		
		});
	}

	var ipObj1 = {
      	callback: function (val) {  //Mandatory
      		updateSelectedDate(val);
        	console.log('Return value from the datepicker popup is : ' + val, new Date(val));
      	},
		inputDate: new Date(),      //Optional
		mondayFirst: true,          //Optional
		closeOnSelect: false,       //Optional
		templateType: 'popup'       //Optional
    };

    function updateSelectedDate(val){
    	var selectedDate = new Date(val);
    	curMonth = selectedDate.getMonth()+1;
		curYear = selectedDate.getFullYear();
		curDay = selectedDate.getDate();
		$scope.currentDate = curDay + " - " + curMonth + " - " +  curYear;
		numOfdays = getNumberOfDays(selectedDate);
		$scope.getStudentHomework();
    }

    function getNumberOfDays(selectedDate){
    	var diff = selectedDate - new Date('1990-01-01 00:00:00');
		return ((((diff / 1000) / 60) / 60) / 24);	
    }
    $scope.openDatePicker = function(){
      	ionicDatePicker.openDatePicker(ipObj1);
    };

    $scope.go_backward = function(){
        numOfdays--;
        
        $scope.getStudentHomework();
    }

    $scope.go_forward = function(){
        numOfdays++;
        $scope.getStudentHomework();
    }
})
