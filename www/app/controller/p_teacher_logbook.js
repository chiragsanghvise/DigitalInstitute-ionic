angular.module('starter')

.controller('PTLogBook', function($scope, $state, $http , $ionicPopup, $ionicLoading, ionicDatePicker, APIUrl,PostCallService) {
//	var api_url="http://school.jmsofttech.com/api/";
	$ionicLoading.show();
	$scope.counter=0;
	$scope.logbook_details = [];
	$scope.logbooks=false;
	$scope.currentDate = "Date";
	$scope.isLogbookDetail = false;
	var days = getNumOfDays(new Date());
	var selectedTeacher = null;
	$http.get(APIUrl+'Teacher')
		.success(function(data){
		
			$ionicLoading.hide();
			if(data!="[]"){
				$scope.isTeacherList=true;
				$scope.isLogbookDetail = false;

				$scope.teacher_list=data;
			}else{
				$scope.isTeacherList=false;
			}
		}).error(function(err){
			$ionicLoading.hide();
			alert("error");
		})

	
	$scope.getTeacherId = function(teacher){
		selectedTeacher = teacher;
		$ionicLoading.show();	
		$scope.isTeacherList=false;
		$scope.isLogBook=true;
		$scope.isLogbookDetail = true;

		$http.get(APIUrl+'ViewTeacherLogBook/'+teacher.Id+'/'+days)
		.success(function(data){
			console.log(data);
			$ionicLoading.hide();
			if(data.length){
				$scope.isLogBook=true;
				$scope.logbook_details=data;
				console.log($scope.logbook_details)
			}else{
				$scope.isLogBook=false;
			}
		}).error(function(err){
			$ionicLoading.hide();
			alert("error");
		})		
	}

	$scope.logbookAccept = function(logbook_data){
		//alert("Hello");
		var post_url=APIUrl+"ApproveTeacherLogbook"; //Chaneg Logbook URL
		logbook_data.IsApproved=1;
		alert(JSON.stringify(logbook_data));
		console.log(JSON.stringify(logbook_data));
		PostCallService.post(logbook_data,post_url).then(function(logbook_status){
			$scope.isAccepted=true;
				$ionicPopup.alert({
					template:logbook_status,
				});
		})

	}

	$scope.logbookReject = function(){
		alert("Rejection");
	}

	$scope.preRecord = function(){
		if($scope.counter>0){
			$scope.counter--;
			$scope.hasNext=true;
		}
		if ($scope.counter==0){
			$scope.hasPre=false;	
		} 
	}

	$scope.nextRecord = function(){
		if($scope.counter<logbook_details.length){
			$scope.counter++;
			$scope.hasPre=true;
		}
		if ($scope.counter==logbook_details.length){
			$scope.hasNext=false;	
		} 
	}

	function getCurrentDate(date){
		var year = date.getFullYear();
        var month = date.getMonth()+1;
        var day = date.getDate();
        return year + "-" + month + "-" + day;
	}

	function getNumOfDays(date){
		date = new Date(date.getFullYear(),date.getMonth(),date.getDate());
		// Assuming date is a string of parsable format: ie. "2014-01-27T01:00:00+00:00"
		var diff = date - new Date('1990-01-01 00:00:00');
		// Calculate from milliseconds
		return ((((diff / 1000) / 60) / 60) / 24);
	}

	var ipObj1 = {
      	callback: function (val) {  //Mandatory
        	var selectedDate = new Date(val);
        	days = getNumOfDays(selectedDate);
        	$scope.currentDate = getCurrentDate(selectedDate);
        	$scope.getTeacherId(selectedTeacher);
      	},
		inputDate: new Date(),      //Optional
		mondayFirst: true,          //Optional
		closeOnSelect: false,       //Optional
		templateType: 'popup'       //Optional
    };

    $scope.openDatePicker = function(){
      	ionicDatePicker.openDatePicker(ipObj1);
    };
})