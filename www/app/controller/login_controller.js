angular.module('starter')

.controller('AppCtrl', function($scope, $state, $ionicPopup, $ionicLoading,$location, PostCallService, $ionicPlatform, $ionicHistory) {
	var loginUrl="Authenticate/AuthorizationToken";
	var BranchPopup;

	$scope.login = function(data) {
		$ionicLoading.show();
	//		$location.path('/dashboard')
		console.log(JSON.stringify(data))
		PostCallService.post(loginUrl,data).then(function(loginData){
			$ionicLoading.hide();
			if (loginData!='error'){
				window.localStorage.setItem('Token', loginData['Token']);
				window.localStorage.setItem('UserType', loginData['UserType']);
				if (loginData['Branches']!=null) {
							
					showBranchList(loginData['Branches']);
					// alert(loginData['Branches'][1]['Name']);	
				}else{
					$location.path('/dashboard')	
					// alert("student");	
				};	
			}else{
				$ionicPopup.alert({
					title: 'Login Failed',
					template: 'Please enter correct login detail'
				});
			}
			
		},function(msg){
			$ionicLoading.hide();
			console.log("ctrl","msg");
			alert("Error");
		});
	};

	var showBranchList=function(Branch){
		$scope.branch_list=Branch;	

		BranchPopup = $ionicPopup.show({
      					templateUrl: 'app/layout/dialog_select_branch.html',
      					title: "Notice",
      					scope: $scope,	
      				});

	};	

	$scope.getBranchId =function(branch){
		window.localStorage.setItem('BranchId', branch.Id);
		$location.path('/dashboard')
		BranchPopup.close();
		
	}

	$ionicPlatform.registerBackButtonAction(function (evt) {
                // prevent default backbutton action
            if($ionicHistory.currentStateName() == 'login'){
                evt.preventDefault();
                ionic.Platform.exitApp(); // stops the app
            }else if($ionicHistory.currentStateName() == 'dashboard'){
                evt.preventDefault();
            }else{
            	$ionicHistory.goBack();
            }
        }, 100);

})
 
