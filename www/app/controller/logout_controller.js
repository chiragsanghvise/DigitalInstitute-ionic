angular.module('starter')

.controller('logoutCtrl', function($scope, $state, $location) {
	window.localStorage.setItem('Token', "");
	window.localStorage.setItem('UserType', "");
	$location.path('/login')
})
 
