angular.module('starter')

.controller('TLogBook', function($scope, $http, $ionicPopup, $ionicLoading, DateTimeService, ionicDatePicker, PostCallService, APIUrl) {
//	var api_url="http://school.jmsofttech.com/api/";
	var _date=9696;
	var _lessonPlan;
	var _teachingAids;
	var _comment;
	//global variables
	var selectedDate = null;
	var selectedBatch = null;
	var selectedLecture = null;
	var diff;
	var days;

	$scope.currentDate = 'Select Date';

	// call service method to get menu_list and pass it to html page
    PostCallService.get('batch').then(function(batches){
        $scope.batchList = batches;
    },function(msg){
        console.log("ctrl","msg");
        alert(msg);
    });

    $scope.getLectureDetails=function(batch){
		// If date is not given, by default today's date is taken.
		if(!selectedDate){
			date = new Date();
		}
        selectedBatch = batch;
		date = new Date(selectedDate.getFullYear(),selectedDate.getMonth(),selectedDate.getDate());
		// Assuming date is a string of parsable format: ie. "2014-01-27T01:00:00+00:00"
		var diff = date - new Date('1990-01-01 00:00:00');
		// Calculate from milliseconds
		days = ((((diff / 1000) / 60) / 60) / 24);
		$ionicLoading.show();
		var url = "ViewMyLectures/" + selectedBatch.Id +"/"+ days; 
		PostCallService.get(url).then(function(data){
			$ionicLoading.hide();
			
			if(data && data.length){
				$scope.hasLecture=true;
				$scope.lectureList=data;
			}else{
				$ionicPopup.alert({
					template:"Lecture Data is not available",					
				});
				$scope.hasLecture=false;
				$scope.hasRecord=false;
			}
		},function(msg){
			$ionicLoading.hide();
			console.log("ctrl","msg");
			alert(msg);
		});
	}


	$scope.getLogBookDetails=function(lecture){	
		selectedLecture = lecture;
		$ionicLoading.show();
		var url = "ViewHomeWork/" + days + "/" + lecture.Id  
		PostCallService.get(url).then(function(data){
			$ionicLoading.hide();
			if(data && data.length){
				$scope.logbook=data;
			}
			$scope.hasRecord=true;
		},function(msg){
			$ionicLoading.hide();
			console.log("ctrl",msg);
		})	
	};

	$scope.saveLogbook=function(logbook){
		_lessonPlan=logbook.LessonPlan;
		_teachingAids=logbook.TeachingAids;
		_comment=logbook.Comment;
		if (_lessonPlan=="") {
			$ionicPopup.alert({
				template:"Please Enter Lesson",					
			});
		}else if (_teachingAids=="") {
			$ionicPopup.alert({
				template:"Please Enter TeachingAids",					
			});
		}else if (_comment=="") {
			$ionicPopup.alert({
				template:"Please Enter comment",					
			})
		}else{
			var _posrUrl="LogBook";	
			var _jsonLogbook='{"Date":'+days+', "LectureId" : ' +selectedLecture.Id + ', "LessonPlan" : "'+_lessonPlan+'"};';
			PostCallService.post(_posrUrl, _jsonLogbook).then(function(status){
				$ionicPopup.alert({
					template:status,					
				});
			})	
		}
	};

	function getCurrentDate(date){
		var year = date.getFullYear();
        var month = date.getMonth();
        var day = date.getDate();
        return year + "-" + month + "-" + day;
	}

	var ipObj1 = {
      	callback: function (val) {  //Mandatory
        	selectedDate = new Date(val);
        	$scope.currentDate = DateTimeService.getDateString(selectedDate);
			if($scope.batchList.length){
        		$scope.hasBatch = true;
			}else{
        		$ionicPopup.alert({
					template:"No data available for given date.",					
				});
			}

      	},
		inputDate: new Date(),      //Optional
		mondayFirst: true,          //Optional
		closeOnSelect: false,       //Optional
		templateType: 'popup'       //Optional
    };

    $scope.openDatePicker = function(){
      	ionicDatePicker.openDatePicker(ipObj1);
    };
})	