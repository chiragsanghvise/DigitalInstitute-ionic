angular.module('starter')

.controller('GraphCtrl', function($scope, $state, $ionicPopup, $window, PostCallService,ionicDatePicker) {
	$scope.startDays=0;
	$scope.endDays=0;
	var monthNames = ["Jan", "Feb", "March", "April", "May", "June",
                "July", "Aug", "Sept", "Oct", "Nov", "Dec"
            ];
	
	function getCurrentDate(date){
		var year = date.getFullYear();
        var month = date.getMonth();
        var day = date.getDate();
        monthName = monthNames[month]
        return day + " " + monthName + ", " + year;
	}


	function dateDiffernce(date){
		if(!date){
			date = new Date();
		}
		date = new Date(date.getFullYear(),date.getMonth(),date.getDate());
		// Assuming date is a string of parsable format: ie. "2014-01-27T01:00:00+00:00"
		var diff = date - new Date('1990-01-01 00:00:00');
		// Calculate from milliseconds
		days = ((((diff / 1000) / 60) / 60) / 24);
		return days;	
	}

	/* start Date Picker */


	var startObj = {
	
      	callback: function (val) {  //Mandatory
        	var selectedDate = new Date(val);
        		// alert($scope.endDays);
      		$scope.startDays=dateDiffernce(selectedDate); 	
 	   	    $scope.startDate = getCurrentDate(selectedDate);  	

        	if($scope.startDays < $scope.endDays ){
				$scope.fetchGraphData();
        	}else{
        			if ($scope.endDays !=0) {
    	    			$ionicPopup.alert({
							template:"Please select correct Start date",					
						});
    	    		};	
        		// alert("Not Ok");
        	}
      	},

		inputDate: new Date(),      //Optional
		mondayFirst: true,          //Optional
		closeOnSelect: false,       //Optional
		templateType: 'popup'       //Optional
    };

	$scope.openStartDatePicker=function(){		      
		ionicDatePicker.openDatePicker(startObj);
	}


	/* end date picker */


	var endObj = {
      	callback: function (val) {  //Mandatory
        	var selectedDate = new Date(val);
        	$scope.endDays=dateDiffernce(selectedDate); 	
        	$scope.endDate = getCurrentDate(selectedDate);	
      		
      		if($scope.startDays < $scope.endDays ){
				$scope.fetchGraphData();
        	}else{
        			if ($scope.startDaysDays !=0) {
    	    			$ionicPopup.alert({
							template:"Please select correct end date",					
						});
    	    		};	
        		// alert("Not Ok");
        	}
      	},
		inputDate: new Date(),      //Optional
		mondayFirst: true,          //Optional
		closeOnSelect: false,       //Optional
		templateType: 'popup'       //Optional
    };

	$scope.openEndDatePicker=function(){
		ionicDatePicker.openDatePicker(endObj);
	}
	
	$scope.fetchGraphData=function(){		
		var type=window.localStorage.getItem('Type');
		var graphUrl="Graph/"+type+"/"+$scope.startDays+"/"+$scope.endDays;
		PostCallService.get(graphUrl).then(function(graphData){

			// alert(JSON.stringify(graphData));
			$scope.labels = graphData.Label;
		    $scope.data = [
		        graphData.Data[0].Values,
		        // graphData.Data[1].Values
		    ];
		})		
	}





})