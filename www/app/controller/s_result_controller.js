angular.module('starter')

.controller('SResultCtrl', function($scope, $state,$http ,$ionicPopup,$ionicLoading,PostCallService,APIUrl) {	
	$ionicLoading.show();
	PostCallService.get('ExamResult').then(function(data){
		$ionicLoading.hide();
		if(JSON.stringify(data)=="[]"){
			$ionicPopup.alert({
				template:"Result Data is not available for this Exam",					
			});
				$scope.result_available=true;
		}else{
				$scope.result_data=data;
				$scope.result_available=false;
			}
	},function(error){
		$ionicLoading.hide();
		alert("error");
	})

	/*$scope.examId = function(item) {
		$ionicLoading.show();
		var examDataUrl="ExamResult";
		PostCallService.get(examDataUrl).then(function(data){
			$ionicLoading.hide();
				if(JSON.stringify(data)=="null"){
					$ionicPopup.alert({
						template:"Result Data is not available for this Exam",					
					});
					$scope.result_available=true;
				}else{
					$scope.result_data=data;
					$scope.result_available=false;
				}
			},function(error){
				$ionicLoading.hide();
				alert(error);
		});
	}*/	 
})