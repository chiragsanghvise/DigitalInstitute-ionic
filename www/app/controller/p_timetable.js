angular.module('starter')

.controller('PTimetable', function($scope, $state,$http ,$ionicPopup,$ionicLoading,APIUrl,PostCallService) {
	// var api_url="http://school.jmsofttech.com/api/";
	$ionicLoading.show();
	PostCallService.get('faculty').then(function(facultyData){
		if(JSON.stringify(facultyData)=="[]"){
			$ionicLoading.hide();	
			$scope.hasFaculty=false;	
			$ionicPopup.alert({
				template:"Faculty data is not available",					
			});			
		}else{
			$ionicLoading.hide();
			$scope.faculty_list=facultyData;
			$scope.hasFaculty=true;
		}
	},function(msg){
		alert("error");
	});

	$scope.facultyId=function(faculty){
		$ionicLoading.show();
		PostCallService.get('TodaySchedule/'+faculty.Id).then(function(data){
			if (JSON.stringify(data)=="[]") {
				$ionicLoading.hide();	
				$scope.hasSchedule=false;	
				$ionicPopup.alert({
					template:"Shcedule data is not available",					
				});	
			}else{
				$ionicLoading.hide();
				$scope.schedule=data;
				$scope.hasSchedule=true;	
			}
		});	
	}
})